const User = require("../models/user");
const bcrypt = require("bcryptjs");
const userRouter = require("express").Router();
const jwt = require('jwt-simple');

// const express = require("express")
// const userRouter = express.Router();

userRouter.post("/register", async (req, res) => {
    try {
        const password = req.body.password;
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(password, salt);

        req.body.password = hashPassword

        let newUser = new User(req.body) 
    
    await newUser.save()
    const token = jwt.encode({id: newUser.id},"authentification")
    res.send({msg:"User successfully added",newUser,token});
}
    catch (error){
        console.error(error)
    }
})

userRouter.post("/login", async (req, res) => {
    try {
    let user = await User.findOne({email : req.body.email})
    const isMatch = await bcrypt.compare(req.body.password, user.password)
    res.send({Msg :"User connected", user})
    }
    catch (error){
        console.error(error)
    }
})

module.exports = userRouter;