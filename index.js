const express = require("express");
const app = express();

const mongoose = require("mongoose");
const passportjwt = require('./middlewares/passportJwt')();
const userRouter = require("./routes/userRoute");

main().catch((err) => console.log(err));

async function main() {
  await mongoose.connect("mongodb://127.0.0.1:27017/auth_db");
  console.log("database connected");
}

const port = process.env.PORT || 5000;

app.use(express.urlencoded({extended:false}))

app.get("/", (req, res) => {
  res.send("tata");
});

app.use(passportjwt.initialize());

app.use("/users", userRouter);

app.listen(port, () => console.log(`server is running ${port}`));
