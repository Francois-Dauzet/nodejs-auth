const passport = require('passport');

const passportjwt = require('passport-jwt');
const User = require('../models/user');

const JwtStrategy = passportjwt.Strategy;
const ExtractJwt = passportjwt.ExtractJwt;

const params = {
    secretOrKey: "authentification",
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};

module.exports = ()=> {
     const strategy =  new JwtStrategy(params, async(payload,done)=>{
        const user = await User.findOne(payload.id);
        if(!user) return done(new Error("User not found"),null)
        return done(null , user)
     })
     passport.use(strategy)
     return {
        initialize : function(){
            return passport.initialize()
        },
        authenticate : function(){
            return passport.authenticate("jwt",{
                session : false
            })
        }
     }
}